import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_arabic_rtl_i18n/login.dart';
import 'package:flutter_arabic_rtl_i18n/scope_model_wrapper.dart';
import 'package:flutter_arabic_rtl_i18n/style.dart';
import 'package:flutter_arabic_rtl_i18n/translation.dart';
import 'package:scoped_model/scoped_model.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<AppModel>(
        builder: (context, child, model) => MaterialApp(
          locale: model.appLocal,
          localizationsDelegates: [
            const TranslationsDelegate(),
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
          ],
          supportedLocales: [
            const Locale('ar', ''), // Arabic
            const Locale('en', ''), // English
          ],
          debugShowCheckedModeBanner: false,
          theme: hrTheme,
          title: "Arabic Flutter",
          home: new Login(),
        ));
  }
}